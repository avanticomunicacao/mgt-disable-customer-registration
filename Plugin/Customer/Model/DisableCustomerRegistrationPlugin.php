<?php

namespace Avanti\DisableCustomerRegistration\Plugin\Customer\Model;

use Magento\Customer\Model\Registration;
use Avanti\DisableCustomerRegistration\Helper\Data as CustomerRegistrationHelper;

class DisableCustomerRegistrationPlugin
{

    protected $customerRegistrationHelper;

    public function __construct(
        CustomerRegistrationHelper $customerRegistrationHelper
    ){
        $this->customerRegistrationHelper = $customerRegistrationHelper;
    }

    public function afterIsAllowed(Registration $subject, $result)
    {
        if ($this->customerRegistrationHelper->isCustomerRegistrationDisabled()) {
            return false;
        }

        return $result;
    }
}