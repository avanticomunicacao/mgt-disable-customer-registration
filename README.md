## module-disable-customer-registration
    This extension allows you to disable customer registration in your Magento store.

## Configuration
1. Stores > Configuration > Customers > Customer Configuration
2. Under **Create New Account Options** tab you will find the **Disable Customer Registration** option
3. Enable this to activate the plugin
