<?php

namespace Avanti\DisableCustomerRegistration\Rewrite\Magento\Company\Block\Company\Register;

use \Magento\Company\Block\Company\Register\Link as RegisterLink;

class Link extends RegisterLink
{
    public function _toHtml()
    {
        if (false != $this->getTemplate()) {
            return parent::_toHtml();
        }

        return '<li><a ' . $this->getLinkAttributes() . ' >' . $this->escapeHtml($this->getLabel()) . '</a></li>';
    }
}