<?php

namespace Avanti\DisableCustomerRegistration\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Avanti\DisableCustomerRegistration\Helper\Config;

class Data extends AbstractHelper
{
    protected $scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig) {
        $this->scopeConfig = $scopeConfig;
    }

    public function isCustomerRegistrationDisabled()
    {
        return $this->scopeConfig->getValue(
            Config::XML_PATH_DISABLE_CUSTOMER_REGISTRATION
        );
    }
}