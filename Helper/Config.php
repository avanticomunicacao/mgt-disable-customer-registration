<?php

namespace Avanti\DisableCustomerRegistration\Helper;

class Config {
    const XML_PATH_DISABLE_CUSTOMER_REGISTRATION = 'customer/create_account/disable_customer_registration';
}